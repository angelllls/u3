#!/bin/bash
sh /etc/systemfile_current_md5.sh

if [ ! -f /data/aurora/system_original_md5.file ];then
    echo "error:no original file,exit."
    exit 1
fi
tmp_current=`md5 /data/aurora/system_original_md5.file`
currentfile_md5=${tmp_current:0:32}

tmp_original=`md5 /data/aurora/system_current_md5.file`
originalfile_md5=${tmp_original:0:32}

echo "current_system_md5:"${currentfile_md5}
echo "original_system_md5:"${originalfile_md5}

if [[ ${currentfile_md5} != ${originalfile_md5} ]];then
    echo "phone root"
    exit 1
else
    echo "reset original file."
    rm /data/aurora/system_original_md5.file
    exit 0
fi

