ifeq ("$(APK_FACEBOOK_SUPPORT)","yes")
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := Facebook
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_CERTIFICATE := platform
prebuilt_maps_PRODUCT_AAPT_CONFIG := $(subst $(comma), ,$(PRODUCT_AAPT_CONFIG))
ifneq (,$(filter xxhdpi,$(prebuilt_maps_PRODUCT_AAPT_CONFIG)))
#build-fb4a-fbandroid.3344689 - (ARMV7, XXHDPI, ICS+).apk
LOCAL_SRC_FILES := Facebook_xxhdpi.apk

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/app/$(LOCAL_SRC_FILES) \
	$(LOCAL_PATH)/xxhdpi_lib/libcryptox.so:system/vendor/lib/libcryptox.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb.so:system/vendor/lib/libfb.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_breakpad_client.so:system/vendor/lib/libfb_breakpad_client.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_cpucapabilities.so:system/vendor/lib/libfb_cpucapabilities.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_crypto.so:system/vendor/lib/libfb_crypto.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_dalvik-internals.so:system/vendor/lib/libfb_dalvik-internals.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_ffmpeg.so:system/vendor/lib/libfb_ffmpeg.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_ffmpeg_jni.so:system/vendor/lib/libfb_ffmpeg_jni.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_filesystem.so:system/vendor/lib/libfb_filesystem.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_imgproc.so:system/vendor/lib/libfb_imgproc.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfbjni.so:system/vendor/lib/libfbjni.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_jpegturbo.so:system/vendor/lib/libfb_jpegturbo.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_libyuv.so:system/vendor/lib/libfb_libyuv.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_libyuv_jni.so:system/vendor/lib/libfb_libyuv_jni.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_qt-faststart_jni.so:system/vendor/lib/libfb_qt-faststart_jni.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_stl_shared.so:system/vendor/lib/libfb_stl_shared.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_tracker.so:system/vendor/lib/libfb_tracker.so \
	$(LOCAL_PATH)/xxhdpi_lib/libfb_xzdecoder.so:system/vendor/lib/libfb_xzdecoder.so \
	$(LOCAL_PATH)/xxhdpi_lib/libgif.so:system/vendor/lib/libgif.so \
	$(LOCAL_PATH)/xxhdpi_lib/libheartbleed.so:system/vendor/lib/libheartbleed.so \
	$(LOCAL_PATH)/xxhdpi_lib/libliger.so:system/vendor/lib/libliger.so \
	$(LOCAL_PATH)/xxhdpi_lib/libmozgluelinker.so:system/vendor/lib/libmozgluelinker.so \
	$(LOCAL_PATH)/xxhdpi_lib/libpeanut.so:system/vendor/lib/libpeanut.so \
	$(LOCAL_PATH)/xxhdpi_lib/libqt-faststart.so:system/vendor/lib/libqt-faststart.so \
	$(LOCAL_PATH)/xxhdpi_lib/libreflex.so:system/vendor/lib/libreflex.so \
	$(LOCAL_PATH)/xxhdpi_lib/libreflex-jni.so:system/vendor/lib/libreflex-jni.so \
	$(LOCAL_PATH)/xxhdpi_lib/libridgebase.so:system/vendor/lib/libridgebase.so \
	$(LOCAL_PATH)/xxhdpi_lib/libstopmotion.so:system/vendor/lib/libstopmotion.so \
	$(LOCAL_PATH)/xxhdpi_lib/libwebp.so:system/vendor/lib/libwebp.so 

else ifneq (,$(filter xhdpi,$(prebuilt_maps_PRODUCT_AAPT_CONFIG)))
#build-fb4a-fbandroid.3344688 - (ARMV7, XHDPI, ICS+).apk
LOCAL_SRC_FILES := Facebook_xhdpi.apk

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/app/$(LOCAL_SRC_FILES) \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libcryptox.so:system/vendor/lib/libcryptox.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb.so:system/vendor/lib/libfb.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_breakpad_client.so:system/vendor/lib/libfb_breakpad_client.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_cpucapabilities.so:system/vendor/lib/libfb_cpucapabilities.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_crypto.so:system/vendor/lib/libfb_crypto.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_dalvik-internals.so:system/vendor/lib/libfb_dalvik-internals.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_ffmpeg.so:system/vendor/lib/libfb_ffmpeg.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_ffmpeg_jni.so:system/vendor/lib/libfb_ffmpeg_jni.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_filesystem.so:system/vendor/lib/libfb_filesystem.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_imgproc.so:system/vendor/lib/libfb_imgproc.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfbjni.so:system/vendor/lib/libfbjni.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_jpegturbo.so:system/vendor/lib/libfb_jpegturbo.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_libyuv.so:system/vendor/lib/libfb_libyuv.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_libyuv_jni.so:system/vendor/lib/libfb_libyuv_jni.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_qt-faststart_jni.so:system/vendor/lib/libfb_qt-faststart_jni.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_stl_shared.so:system/vendor/lib/libfb_stl_shared.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_tracker.so:system/vendor/lib/libfb_tracker.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_xzdecoder.so:system/vendor/lib/libfb_xzdecoder.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libgif.so:system/vendor/lib/libgif.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libheartbleed.so:system/vendor/lib/libheartbleed.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libliger.so:system/vendor/lib/libliger.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libmozgluelinker.so:system/vendor/lib/libmozgluelinker.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libpeanut.so:system/vendor/lib/libpeanut.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libqt-faststart.so:system/vendor/lib/libqt-faststart.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libreflex.so:system/vendor/lib/libreflex.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libreflex-jni.so:system/vendor/lib/libreflex-jni.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libridgebase.so:system/vendor/lib/libridgebase.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libstopmotion.so:system/vendor/lib/libstopmotion.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libwebp.so:system/vendor/lib/libwebp.so 

else ifneq (,$(filter hdpi,$(prebuilt_maps_PRODUCT_AAPT_CONFIG)))
#build-fb4a-fbandroid.3344686 - (ARMV7, HDPI, ICS+).apk
LOCAL_SRC_FILES := Facebook_hdpi.apk

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/app/$(LOCAL_SRC_FILES) \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libcryptox.so:system/vendor/lib/libcryptox.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb.so:system/vendor/lib/libfb.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_breakpad_client.so:system/vendor/lib/libfb_breakpad_client.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_cpucapabilities.so:system/vendor/lib/libfb_cpucapabilities.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_crypto.so:system/vendor/lib/libfb_crypto.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_dalvik-internals.so:system/vendor/lib/libfb_dalvik-internals.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_ffmpeg.so:system/vendor/lib/libfb_ffmpeg.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_ffmpeg_jni.so:system/vendor/lib/libfb_ffmpeg_jni.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_filesystem.so:system/vendor/lib/libfb_filesystem.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_imgproc.so:system/vendor/lib/libfb_imgproc.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfbjni.so:system/vendor/lib/libfbjni.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_jpegturbo.so:system/vendor/lib/libfb_jpegturbo.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_libyuv.so:system/vendor/lib/libfb_libyuv.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_libyuv_jni.so:system/vendor/lib/libfb_libyuv_jni.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_qt-faststart_jni.so:system/vendor/lib/libfb_qt-faststart_jni.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_stl_shared.so:system/vendor/lib/libfb_stl_shared.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_tracker.so:system/vendor/lib/libfb_tracker.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libfb_xzdecoder.so:system/vendor/lib/libfb_xzdecoder.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libgif.so:system/vendor/lib/libgif.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libheartbleed.so:system/vendor/lib/libheartbleed.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libliger.so:system/vendor/lib/libliger.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libmozgluelinker.so:system/vendor/lib/libmozgluelinker.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libpeanut.so:system/vendor/lib/libpeanut.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libqt-faststart.so:system/vendor/lib/libqt-faststart.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libreflex.so:system/vendor/lib/libreflex.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libreflex-jni.so:system/vendor/lib/libreflex-jni.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libridgebase.so:system/vendor/lib/libridgebase.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libstopmotion.so:system/vendor/lib/libstopmotion.so \
	$(LOCAL_PATH)/hdpi_xhdpi_lib/libwebp.so:system/vendor/lib/libwebp.so 
else
#build-fb4a-fbandroid.3344678 - (fallback)
LOCAL_SRC_FILES := Facebook_alldpi.apk

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/app/$(LOCAL_SRC_FILES) \
	$(LOCAL_PATH)/alldpi_lib/libcryptox.so:system/vendor/lib/libcryptox.so \
	$(LOCAL_PATH)/alldpi_lib/libfb.so:system/vendor/lib/libfb.so \
	$(LOCAL_PATH)/alldpi_lib/libfb_breakpad_client.so:system/vendor/lib/libfb_breakpad_client.so \
	$(LOCAL_PATH)/alldpi_lib/libfb_cpucapabilities.so:system/vendor/lib/libfb_cpucapabilities.so \
	$(LOCAL_PATH)/alldpi_lib/libfb_crypto.so:system/vendor/lib/libfb_crypto.so \
	$(LOCAL_PATH)/alldpi_lib/libfb_dalvik-internals.so:system/vendor/lib/libfb_dalvik-internals.so \
	$(LOCAL_PATH)/alldpi_lib/libfb_filesystem.so:system/vendor/lib/libfb_filesystem.so \
	$(LOCAL_PATH)/alldpi_lib/libfb_imgproc.so:system/vendor/lib/libfb_imgproc.so \
	$(LOCAL_PATH)/alldpi_lib/libfbjni.so:system/vendor/lib/libfbjni.so \
	$(LOCAL_PATH)/alldpi_lib/libfb_jpegturbo.so:system/vendor/lib/libfb_jpegturbo.so \
	$(LOCAL_PATH)/alldpi_lib/libfb_qt-faststart_jni.so:system/vendor/lib/libfb_qt-faststart_jni.so \
	$(LOCAL_PATH)/alldpi_lib/libfb_stl_shared.so:system/vendor/lib/libfb_stl_shared.so \
	$(LOCAL_PATH)/alldpi_lib/libfb_tracker.so:system/vendor/lib/libfb_tracker.so \
	$(LOCAL_PATH)/alldpi_lib/libgif.so:system/vendor/lib/libgif.so \
	$(LOCAL_PATH)/alldpi_lib/libheartbleed.so:system/vendor/lib/libheartbleed.so \
	$(LOCAL_PATH)/alldpi_lib/libliger.so:system/vendor/lib/libliger.so \
	$(LOCAL_PATH)/alldpi_lib/libpeanut.so:system/vendor/lib/libpeanut.so \
	$(LOCAL_PATH)/alldpi_lib/libqt-faststart.so:system/vendor/lib/libqt-faststart.so \
	$(LOCAL_PATH)/alldpi_lib/libreflex.so:system/vendor/lib/libreflex.so \
	$(LOCAL_PATH)/alldpi_lib/libreflex-jni.so:system/vendor/lib/libreflex-jni.so \
	$(LOCAL_PATH)/alldpi_lib/libridgebase.so:system/vendor/lib/libridgebase.so \
	$(LOCAL_PATH)/alldpi_lib/libstopmotion.so:system/vendor/lib/libstopmotion.so \
	$(LOCAL_PATH)/alldpi_lib/libwebp.so:system/vendor/lib/libwebp.so 
endif

endif

#gionee yuanqingqing 20130123 modify for CR00767444 begin
ifeq ("$(APK_FACEBOOK_SUPPORT)","yes_data")
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := Facebook
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

prebuilt_maps_PRODUCT_AAPT_CONFIG := $(subst $(comma), ,$(PRODUCT_AAPT_CONFIG))
ifneq (,$(filter xxhdpi,$(prebuilt_maps_PRODUCT_AAPT_CONFIG)))
LOCAL_SRC_FILES := Facebook_xxhdpi.apk
else ifneq (,$(filter xhdpi,$(prebuilt_maps_PRODUCT_AAPT_CONFIG)))
LOCAL_SRC_FILES := Facebook_xhdpi.apk
else ifneq (,$(filter hdpi,$(prebuilt_maps_PRODUCT_AAPT_CONFIG)))
LOCAL_SRC_FILES := Facebook_hdpi.apk
else
LOCAL_SRC_FILES := Facebook_alldpi.apk
endif


PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/$(LOCAL_SRC_FILES):data/app/$(LOCAL_SRC_FILES) \
	$(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/gn_app_bk/$(LOCAL_SRC_FILES) 

endif
#gionee yuanqingqing 20130123 modify for CR00767444 end

#gionee lijinfang 2015-3-22 modify for CR01456635 begin
ifeq ("$(APK_FACEBOOK_SUPPORT)","yes_tcard")
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := Facebook
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

prebuilt_maps_PRODUCT_AAPT_CONFIG := $(subst $(comma), ,$(PRODUCT_AAPT_CONFIG))
ifneq (,$(filter xxhdpi,$(prebuilt_maps_PRODUCT_AAPT_CONFIG)))
LOCAL_SRC_FILES := Facebook_xxhdpi.apk
else ifneq (,$(filter xhdpi,$(prebuilt_maps_PRODUCT_AAPT_CONFIG)))
LOCAL_SRC_FILES := Facebook_xhdpi.apk
else ifneq (,$(filter hdpi,$(prebuilt_maps_PRODUCT_AAPT_CONFIG)))
LOCAL_SRC_FILES := Facebook_hdpi.apk
else
LOCAL_SRC_FILES := Facebook_alldpi.apk
endif


PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/$(LOCAL_SRC_FILES):tcard/system/gn_app_bk/$(LOCAL_MODULE)/$(LOCAL_SRC_FILES) 

endif
#gionee lijinfang 2015-3-22 modify for CR01456635 end


ifeq ("$(APK_FACEBOOK_SUPPORT)","yes_mtk")
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := Facebook
LOCAL_MODULE_TAGS := optional
prebuilt_maps_PRODUCT_AAPT_CONFIG := $(subst $(comma), ,$(PRODUCT_AAPT_CONFIG))
ifneq (,$(filter xxhdpi,$(prebuilt_maps_PRODUCT_AAPT_CONFIG)))
LOCAL_SRC_FILES := Facebook_xxhdpi.apk
else ifneq (,$(filter xhdpi,$(prebuilt_maps_PRODUCT_AAPT_CONFIG)))
LOCAL_SRC_FILES := Facebook_xhdpi.apk
else ifneq (,$(filter hdpi,$(prebuilt_maps_PRODUCT_AAPT_CONFIG)))
LOCAL_SRC_FILES := Facebook_hdpi.apk
else
LOCAL_SRC_FILES := Facebook_alldpi.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_CERTIFICATE := platform
endif

LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/vendor/operator/app/$(LOCAL_SRC_FILES)
endif


ifeq ("$(APK_FACEBOOK_SUPPORT)","181")
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := Facebook
LOCAL_SRC_FILES := fbandroid-1.8.1-preload.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := platform
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/fbandroid-1.8.1-preload.apk:system/app/fbandroid-1.8.1-preload.apk
endif


#gionee lijinfang 20140114 add begin
ifeq ("$(GN3RD_FACEBOOK_NEW_VER_SUPPORT)","yes_mtk")
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := Facebook
LOCAL_SRC_FILES :=  Facebook-katana-1.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/Facebook-katana-1.apk:system/vendor/operator/app/Facebook-katana-1.apk \
	$(LOCAL_PATH)/libcryptox.so:system/vendor/lib/libcryptox.so \
	$(LOCAL_PATH)/libfb.so:system/vendor/lib/libfb.so \
	$(LOCAL_PATH)/libfb_breakpad_client.so:system/vendor/lib/libfb_breakpad_client.so \
	$(LOCAL_PATH)/libfb_cpucapabilities.so:system/vendor/lib/libfb_cpucapabilities.so \
	$(LOCAL_PATH)/libfb_crypto.so:ssystem/vendor/lib/libfb_crypto.so \
	$(LOCAL_PATH)/libfb_dalvik-internals.so:system/vendor/lib/libfb_dalvik-internals.so \
	$(LOCAL_PATH)/libfb_filesystem.so:system/vendor/lib/libfb_filesystem.so \
	$(LOCAL_PATH)/libfb_jpegcodec.so:system/vendor/lib/libfb_jpegcodec.so \
	$(LOCAL_PATH)/libfb_jpegturbo.so:system/vendor/lib/libfb_jpegturbo.so \
	$(LOCAL_PATH)/libfb_stl_shared.so:system/vendor/lib/libfb_stl_shared.so \
	$(LOCAL_PATH)/libfb_tracker.so:system/vendor/lib/libfb_tracker.so \
	$(LOCAL_PATH)/libfb_xzdecoder.so:system/vendor/lib/libfb_xzdecoder.so \
	$(LOCAL_PATH)/libfbjni.so:system/vendor/lib/libfbjni.so \
	$(LOCAL_PATH)/libmozgluelinker.so:system/vendor/lib/libmozgluelinker.so \
	$(LOCAL_PATH)/libpeanut.so:system/vendor/lib/libpeanut.so \
	$(LOCAL_PATH)/libreflex-jni.so:system/vendor/lib/libreflex-jni.so \
	$(LOCAL_PATH)/libreflex.so:system/vendor/lib/libreflex.so \
	$(LOCAL_PATH)/librsjni.so:system/vendor/lib/librsjni.so \
	$(LOCAL_PATH)/libRSSupport.so:system/vendor/lib/libRSSupport.so \
	$(LOCAL_PATH)/libstopmotion.so:system/vendor/lib/libstopmotion.so \
	$(LOCAL_PATH)/libwebp.so:system/vendor/lib/libwebp.so
endif
#gionee lijinfang 20140114 add end



##GIONEE heling 20140623 modify  begin
ifeq ("$(GN3RD_FACEBOOK_NEW_VER_SUPPORT)","yes_data")
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := Facebook-katana-1
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := platform
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):data/app/$(LOCAL_SRC_FILES) \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/gn_app_bk/$(LOCAL_SRC_FILES)

endif
##GIONEE heling 20140623 modify  end

