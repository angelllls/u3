ifeq ("$(APK_GAMEZONE_SUPPORT)","yes")
LOCAL_PATH := $(call my-dir)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/GameZone.apk:system/app/GameZone.apk \
    $(LOCAL_PATH)/lib/armeabi-v7a/libApkPatchLibrary.so:system/lib/libApkPatchLibrary.so \
    $(LOCAL_PATH)/lib/armeabi-v7a/liblptcpjin.so:system/lib/liblptcpjin.so

endif
