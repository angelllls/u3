#ifndef TA3D_ITERATOR_H
#define TA3D_ITERATOR_H

#include <cassert>
#include <cstddef>
#include <iterator>

namespace TouchlessA3D {
  /**
   * Iterator wrapping another iterator with type erasure.
   *
   * This iterator supports
   * <ul>
   * <li>assignment</li>
   * <li>copying</li>
   * <li>dereferencing</li>
   * <li>equality</li>
   * <li>incrementation</li>
   * <li>member access</li>
   * <li>non-equality</li>
   * </ul>
   * @tparam T The type of the elements iterated over.
   */
  template<typename T>
  class Iterator {
  public:
    typedef T value_type;
    typedef T* pointer;
    typedef T& reference;
    typedef std::ptrdiff_t difference_type;
    typedef std::input_iterator_tag iterator_category;

    /**
     * Constructs a new Iterator wrapping another iterator.
     *
     * @note If two Iterators are compared without one of them being reachable
     * from the other, the behavior is undefined.
     *
     * @tparam InnerIterator The type of the wrapped iterator. It must support
     *                       copying, equality, incrementation and member
     *                       access.
     * @param innerIterator The wrapped iterator.
     */
    template<typename InnerIterator>
    explicit Iterator(const InnerIterator& innerIterator) :
      m_pConcept(new Model<InnerIterator>(innerIterator))
    {
    }

    /**
     * Copies this iterator.
     */
    Iterator(const Iterator& other) :
      m_pConcept(other.m_pConcept->clone())
    {
      assert(*this == other);
    }

    /**
     * Destructs this iterator.
     */
    ~Iterator() {
      delete m_pConcept;
    }

    /**
     * Assigns to this iterator.
     */
    Iterator& operator=(const Iterator& other) {
      if (*this != other) {
        delete m_pConcept;
        m_pConcept = other.m_pConcept->clone();
      }
      assert(*this == other);
    }

    /**
     * Increments this iterator (prefix operator).
     *
     * @return This iterator.
     */
    Iterator& operator++() {
      m_pConcept->increment();
      return *this;
    }

    /**
     * Increments this iterator (postfix operator).
     *
     * @return This iterator.
     */
    Iterator operator++(int) {
      Iterator tmp(*this);
      operator++();
      return tmp;
    }

    /**
     * Dereferences this iterator.
     *
     * @return The element currently referenced to by this iterator.
     */
    T& operator*() {
      return m_pConcept->dereference();
    }

    /**
     * Provides access to the members of the currently referenced element.
     *
     * @return A pointer to the currently referenced element.
     */
    T* operator->() {
      return m_pConcept->memberAccess();
    }

    /**
     * Checks if two Iterators are equal.
     *
     * @return True if the iterators are equal, false otherwise.
     */
    friend bool operator==(const Iterator& lhs, const Iterator& rhs) {
      return lhs.m_pConcept->equals(rhs.m_pConcept);
    }

    /**
     * Checks if two Iterators are not equal.
     *
     * @return True if the iterators are not equal, false otherwise.
     */
    friend bool operator!=(const Iterator& lhs, const Iterator& rhs) {
      return !(lhs == rhs);
    }

  private:
    struct Concept {
      virtual Concept* clone() const = 0;
      virtual ~Concept() {};
      virtual void increment() = 0;
      virtual T& dereference() const = 0;
      virtual T* memberAccess() const = 0;
      virtual bool equals(const Concept* const) const = 0;
    };

    template<class InnerIterator>
    class Model: public Concept {
    public:
      explicit Model(const InnerIterator& inner) :
        m_pInner(new InnerIterator(inner))
      {
      }

      virtual ~Model() {
        delete m_pInner;
      }

      virtual Concept* clone() const {
        return new Model(*this);
      }

      virtual void increment() {
        ++(*m_pInner);
      }

      virtual T& dereference() const {
        return *(*m_pInner);
      }

      virtual T* memberAccess() const {
        return m_pInner->operator->();
      }

      virtual bool equals(const Concept* const pOther) const {
        /* This unsafe downcasting is done because proper, dynamic downcasting
         * is costly. If one of the compared iterators is reachable from the
         * other, the result of the static_cast is equal to the result of a
         * dynamic_cast.
         *
         * See §24.1/6 [lib.iterator.requirements] in the C++98 standard for the
         * definition of "reachable".
         */
        const void* const pTmpOther(pOther);
        const Model<InnerIterator>* const pModel(static_cast<const Model<InnerIterator>*>(pTmpOther));
        return pModel && (*(pModel->m_pInner) == *m_pInner);
      }

    private:
      // Copying is only accessible through cloning.
      Model(const Model& other) :
        m_pInner(new InnerIterator(*other.m_pInner))
      {
      }

      // The model is not assignable.
      Model& operator=(const Model&);

      /* This must be a pointer or the size of Iterator will change with the
       * size of the wrapped iterator, making Iterator unsuitable for API use.
       */
      InnerIterator* const m_pInner;
    };

    Concept* m_pConcept;
  };
}

#endif  // TA3D_ITERATOR_H
