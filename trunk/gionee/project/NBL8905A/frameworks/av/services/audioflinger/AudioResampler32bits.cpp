#define LOG_TAG "SRCSINC"
#define LOG_NDEBUG 0
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <cutils/log.h>
#include <math.h>
#include "AudioResampler.h"
#include "AudioResampler32bits.h"
#include <time.h>
#include "filter44k.h"

//#define VERY_VERY_VERBOSE_LOGGING
#ifdef VERY_VERY_VERBOSE_LOGGING
#define ALOGVV ALOGV
#else
#define ALOGVV(a...) do { } while(0)
#endif

#define INT32_MAX		(2147483647)
#define FFMIN(a,b) ((a) > (b) ? (b) : (a))
#define FFMAX(a,b) ((a) > (b) ? (a) : (b))
#define FFALIGN(x, a) (((x)+(a)-1)&~((a)-1))
#define FFABS(a) ((a) >= 0 ? (a) : (-(a)))
#define FILTER_SHIFT 30
#define OUT(d, v) v = (v + (1<<(FILTER_SHIFT-1)))>>FILTER_SHIFT; \
                  d = (uint64_t)(v + 0x80000000) > 0xFFFFFFFF ?  \
	            	(v>>63) ^ 0x7FFFFFFF : v


namespace android {
// ----------------------------------------------------------------------------

typedef struct AVRational{
    int num; ///< numerator
    int den; ///< denominator
} AVRational;

int64_t av_gcd(int64_t a, int64_t b)
{
    if (b)
        return av_gcd(b, a % b);
    else
        return a;
}
	
int32_t AudioResampler32bits::av_clipl_int32_c(int64_t a)
{
    if ((a+0x80000000u) & ~(0xFFFFFFFF))
		return (int32_t)((a>>63) ^ 0x7FFFFFFF);
    else
		return (int32_t)a;
}

int32_t AudioResampler32bits::av_reduce(int &dst_num, int &dst_den,
              int64_t num, int64_t den, int64_t max)
{
    AVRational a0 = { 0, 1 }, a1 = { 1, 0 };
    int sign = (num < 0) ^ (den < 0);
    int64_t gcd = av_gcd(FFABS(num), FFABS(den));

    if (gcd) {
        num = FFABS(num) / gcd;
        den = FFABS(den) / gcd;
    }
    if (num <= max && den <= max) {
        a1 = (AVRational) { num, den };
        den = 0;
    }

    while (den) {
        uint64_t x        = num / den;
        int64_t next_den  = num - den * x;
        int64_t a2n       = x * a1.num + a0.num;
        int64_t a2d       = x * a1.den + a0.den;

        if (a2n > max || a2d > max) {
            if (a1.num) x =          (max - a0.num) / a1.num;
            if (a1.den) x = FFMIN(x, (max - a0.den) / a1.den);

            if (den * (2 * x * a1.den + a0.den) > num * a1.den)
                a1 = (AVRational) { x * a1.num + a0.num, x * a1.den + a0.den };
            break;
        }

        a0  = a1;
        a1  = (AVRational) { a2n, a2d };
        num = den;
        den = next_den;
    }
    av_gcd(a1.num, a1.den);

    dst_num = sign ? -a1.num : a1.num;
    dst_den = a1.den;

    return den == 0;
}

	
double AudioResampler32bits::bessel(double x){
    double v=1;
    double lastv=0;
    double t=1;
    int i;
    static const double inv[100]={
 1.0/( 1* 1), 1.0/( 2* 2), 1.0/( 3* 3), 1.0/( 4* 4), 1.0/( 5* 5),
 1.0/( 6* 6), 1.0/( 7* 7), 1.0/( 8* 8), 1.0/( 9* 9), 1.0/(10*10),
 1.0/(11*11), 1.0/(12*12), 1.0/(13*13), 1.0/(14*14), 1.0/(15*15),
 1.0/(16*16), 1.0/(17*17), 1.0/(18*18), 1.0/(19*19), 1.0/(20*20),
 1.0/(21*21), 1.0/(22*22), 1.0/(23*23), 1.0/(24*24), 1.0/(25*25),
 1.0/(26*26), 1.0/(27*27), 1.0/(28*28), 1.0/(29*29), 1.0/(30*30),
 1.0/(31*31), 1.0/(32*32), 1.0/(33*33), 1.0/(34*34), 1.0/(35*35),
 1.0/(36*36), 1.0/(37*37), 1.0/(38*38), 1.0/(39*39), 1.0/(40*40),
 1.0/(41*41), 1.0/(42*42), 1.0/(43*43), 1.0/(44*44), 1.0/(45*45),
 1.0/(46*46), 1.0/(47*47), 1.0/(48*48), 1.0/(49*49), 1.0/(50*50),
 1.0/(51*51), 1.0/(52*52), 1.0/(53*53), 1.0/(54*54), 1.0/(55*55),
 1.0/(56*56), 1.0/(57*57), 1.0/(58*58), 1.0/(59*59), 1.0/(60*60),
 1.0/(61*61), 1.0/(62*62), 1.0/(63*63), 1.0/(64*64), 1.0/(65*65),
 1.0/(66*66), 1.0/(67*67), 1.0/(68*68), 1.0/(69*69), 1.0/(70*70),
 1.0/(71*71), 1.0/(72*72), 1.0/(73*73), 1.0/(74*74), 1.0/(75*75),
 1.0/(76*76), 1.0/(77*77), 1.0/(78*78), 1.0/(79*79), 1.0/(80*80),
 1.0/(81*81), 1.0/(82*82), 1.0/(83*83), 1.0/(84*84), 1.0/(85*85),
 1.0/(86*86), 1.0/(87*87), 1.0/(88*88), 1.0/(89*89), 1.0/(90*90),
 1.0/(91*91), 1.0/(92*92), 1.0/(93*93), 1.0/(94*94), 1.0/(95*95),
 1.0/(96*96), 1.0/(97*97), 1.0/(98*98), 1.0/(99*99), 1.0/(10000)
    };

    x= x*x/4;
    for(i=0; v != lastv; i++){
        lastv=v;
        t *= x*inv[i];
        v += t;        
    }
    return v;
}
int AudioResampler32bits::build_filter(){
    int ph, i;
    double x, y, w;
    double *tab = new double[tap_count * sizeof(*tab)];
	    const int center= (tap_count-1)/2;
	int kaiser_beta = 9;

	if(!tab)
		return -1;
    
    /* if upsampling, only need to interpolate, no filter */
    if (factor > 1.0)
        factor = 1.0;

    for(ph=0;ph<phase_count;ph++) {
        double norm = 0;
        for(i=0;i<tap_count;i++) {
            x = M_PI * ((double)(i - center) - (double)ph / phase_count) * factor;
            if (x == 0) y = 1.0;
            else        y = sin(x) / x;
            
			w = 2.0*x / (factor*tap_count*M_PI);
			y *= bessel(kaiser_beta*sqrt(FFMAX(1-w*w, 0)));
 
            tab[i] = y;
            norm += y;
        }
	
        /* normalize so that an uniform color remains the same */
        
	for(i=0;i<tap_count;i++)
		mFilter[ph * alloc + i] =
			av_clipl_int32_c(llrint(tab[i] * scale / norm));
	}
	
	delete tab;
	return 0;
}

void AudioResampler32bits::setSampleRate(int32_t inSampleRate) {
	double cutoff = 1;

	if(mFilter)
		return;
	ALOGI("Rate:%d", inSampleRate);
    mInSampleRate = inSampleRate;
    mPhaseIncrement = (uint32_t)((kPhaseMultiplier * inSampleRate) / mSampleRate);
	factor= FFMIN(mSampleRate * cutoff / inSampleRate, 1.0);
	tap_count = FFMAX((int)ceil(filter_size/factor), 1);
	filter_alloc = FFALIGN(tap_count, 8);
	alloc  = FFALIGN(tap_count, 8);
   
	if(!mFilter){
		if(inSampleRate == 44100 && phase_shift == 14){
			mFilter = (int32_t *) filter44k;
		}else{
			ALOGVV("new mFilter");
			mFilter = new  int32_t[(phase_count+1) * alloc];
			if(mFilter){
				build_filter();
				memcpy((void *)mFilter + (alloc * phase_count + 1) * 4, (void *)mFilter, (alloc - 1) * 4);
				memcpy((void *)mFilter + (alloc * phase_count) * 4,  (void *)mFilter + (alloc - 1) * 4, 4);
			}else{
				ALOGE("filter new error!");
			}
		}
	}
	
	av_reduce(src_incr, dst_incr, mSampleRate,
			  inSampleRate* (int64_t)phase_count, INT32_MAX/2);
	ideal_dst_incr = dst_incr;
    dst_incr_div   = dst_incr / src_incr;
    dst_incr_mod   = dst_incr % src_incr;
	ALOGVV("src_incr:%#x,dst_incr:%#x,ideal_dst_incr:%#x,dst_incr_div:%#x,dst_incr_mod:%#x",
		  src_incr, dst_incr, ideal_dst_incr, dst_incr_div, dst_incr_mod);
}	
	
int AudioResampler32bits::resampleInt32Stereo(
                                   int32_t *dest, const int32_t *sourceL,
								   const int32_t *sourceR, int n){
    int32_t *dst = dest;
    const int32_t *srcL = sourceL;
	const int32_t *srcR = sourceR;
    int dst_index;
    int index= mIndex;
    int frac= mFrac;
    int sample_index = index >> phase_shift;
	int32_t phase_mask    = phase_count - 1;

	if(!mFilter){
		ALOGE("filter is NUL!");
		return 0;
	} 
	
    index &= phase_mask;
    for (dst_index = 0; dst_index < n; dst_index += 2) {
        int32_t *filter = mFilter + filter_alloc * index;
        int64_t valL=0;
		int64_t valR=0;

        int i;
        for (i = 0; i < tap_count; ++i) {
            valL += srcL[sample_index + i] * (int64_t)filter[i];
			valR += srcR[sample_index + i] * (int64_t)filter[i];
        }

        OUT(dst[dst_index], valL);
		OUT(dst[dst_index + 1], valR);

        frac += dst_incr_mod;
        index += dst_incr_div;
        if (frac >= src_incr) {
            frac -= src_incr;
            index++;
        }
        sample_index += index >> phase_shift;
        index &= phase_mask;
    }

	mFrac= frac;
	mIndex= index;

    return sample_index;
}

int AudioResampler32bits::resampleInt32Mono(
                                   int32_t *dest, const int32_t *sourceL,
								   int n){
    int32_t *dst = dest;
    const int32_t *srcL = sourceL;
    int dst_index;
    int index= mIndex;
    int frac= mFrac;
    int sample_index = index >> phase_shift;
	int32_t phase_mask    = phase_count - 1;

	if(!mFilter)
		return 0;
	
    index &= phase_mask;
    for (dst_index = 0; dst_index < n; dst_index += 2) {
        int32_t *filter = mFilter + filter_alloc * index;
        int64_t valL=0;
	

        int i;
        for (i = 0; i < tap_count; ++i) {
            valL += srcL[sample_index + i] * (int64_t)filter[i];
		}

        OUT(dst[dst_index], valL);
		OUT(dst[dst_index + 1], valL);
		
        frac += dst_incr_mod;
        index += dst_incr_div;
        if (frac >= src_incr) {
            frac -= src_incr;
            index++;
        }
        sample_index += index >> phase_shift;
        index &= phase_mask;
    }

	mFrac= frac;
	mIndex= index;

    return sample_index;
}
	
void AudioResampler32bits::init() {
   mFrac= 0;
   mIndex= 0;
   mconsumed = 0;
   ALOGVV("init");
}

void AudioResampler32bits::reset(){
   mFrac= 0;
   mIndex= 0;
   mconsumed = 0;
   ALOGVV("reset");
}

void AudioResampler32bits::resample(int32_t* out, size_t outFrameCount,
        AudioBufferProvider* provider) {
    // should never happen, but we overflow if it does
	// select the appropriate resampler
    switch (mChannelCount) {
    case 1:		
        resampleMono32(out, outFrameCount, provider);
        break;
    case 2:
        resampleStereo32(out, outFrameCount, provider);
        break;
    }
#ifdef AUDIORESAMPLEER_PCM_DUMP
	FILE *fp;
	if ((fp = fopen("/data/src.cap", "a+")) != NULL) {
		fwrite((uint8_t *)out, 1, outFrameCount * 4 * 2, fp);
		fclose(fp);
	}else{
		//	ALOGVV("src.cap OPEN FAILED");
	}
#endif
}

int64_t AudioResampler32bits::calculateInputPTS(int outputFrameIndex) {
    if (mPTS == AudioBufferProvider::kInvalidPTS) {
        return AudioBufferProvider::kInvalidPTS;
    } else {
        return mPTS + ((outputFrameIndex * mLocalTimeFreq) / mInSampleRate);
    }
}


void AudioResampler32bits::resampleStereo32(int32_t* out,
											size_t outFrameCount,
											AudioBufferProvider* provider) {
    size_t inputIndex = mInputIndex;
    uint32_t phaseFraction = mPhaseFraction;
    uint32_t phaseIncrement = mPhaseIncrement;
    size_t outputIndex = 0;
    size_t inFrameCount= (outFrameCount * mInSampleRate) / mSampleRate;
	size_t inIndex = 0;
	int i;
 	
   	if(mInSampleRate <  mSampleRate)
		inFrameCount = outFrameCount + 32;
	
	if(!inBufferL)
		inBufferL =  new int32_t[inFrameCount];
	if(!inBufferL)
		return;
	
	if(!inBufferR)
		inBufferR =  new int32_t[inFrameCount];
	if(!inBufferR)
		return;
	
	if(mconsumed)
		inIndex = inFrameCount- mconsumed;
	else
		inIndex = 0;
	
	while(inIndex < inFrameCount){
		mBuffer.frameCount = inFrameCount - inIndex;
		provider->getNextBuffer(&mBuffer,
								calculateInputPTS(inFrameCount - inIndex));
		if (mBuffer.raw == NULL) {
			mconsumed = 0;
			if(inIndex > 0){
				ALOGVV("stereo:padding with zero");
				for(i = inIndex; i < inFrameCount; ++i){
					inBufferL[inIndex] = 0;
					inBufferR[inIndex] = 0;
				}
				break;
			}else{
				ALOGW("stereo:NO DATA TO SRC!");
				return;
			}
		}
		if(mBitDepth == 24){
			uint8_t *in8 =(uint8_t *)mBuffer.i8;
			for(i = 0; i < mBuffer.frameCount; ++i){
				inBufferL[i + inIndex] = (int32_t) (in8[0] << 8 |
													(in8[1] << 16) |
													(in8[2] << 24));
 				
				inBufferR[i + inIndex] = (int32_t) (in8[3] << 8 |
													(in8[4] << 16) |
													(in8[5] << 24));
				in8 += 6;
			}
		}else if(mBitDepth == 16){
			int16_t *in16 = mBuffer.i16;
			for(i = 0; i < mBuffer.frameCount; ++i){
				inBufferL[i + inIndex] = (int32_t) (*in16++) << 16 ;
				inBufferR[i + inIndex] = (int32_t) (*in16++) << 16 ;
			}
#ifdef AUDIORESAMPLEER_PCM_DUMP
			FILE *fp;
			in16 = mBuffer.i16;
			if ((fp = fopen("/data/src0.cap", "a+")) != NULL) {
				fwrite((uint8_t *)in16, 1, mBuffer.frameCount * 2 * 2, fp);
				fclose(fp);
			}else{
				//ALOGVV("src0.cap OPEN FAILED");
			}
#endif
		}else if(mBitDepth == 32){
			int32_t *in32 = mBuffer.i32;
			for(i = 0; i < mBuffer.frameCount; ++i){
				inBufferL[i + inIndex] = (int32_t) (*in32++);
				inBufferR[i + inIndex] = (int32_t) (*in32++);
			}
		}
		
		inIndex += mBuffer.frameCount;
		ALOGVV("stereo:GET frames:%d,inIndex:%d", mBuffer.frameCount,inIndex);
		provider->releaseBuffer(&mBuffer);
	}
	mconsumed =
	resampleInt32Stereo(out, inBufferL, inBufferR, outFrameCount * 2);
	ALOGVV("mconsumed:%d", mconsumed);

#ifdef AUDIORESAMPLEER_PCM_DUMP
	FILE *fp;
	if ((fp = fopen("/data/src0l.cap", "a+")) != NULL) {
		fwrite((uint8_t *)inBufferL, 1, mconsumed * 4, fp);
		fclose(fp);
	}else{
			//ALOGVV("src0l.cap OPEN FAILED");
	}
	if ((fp = fopen("/data/src0r.cap", "a+")) != NULL) {
		fwrite((uint8_t *)inBufferR, 1, mconsumed * 4, fp);
		fclose(fp);
	}else{
			//ALOGVV("src0r.cap OPEN FAILED");
	}
	
#endif
	memcpy(inBufferL, inBufferL + mconsumed, (inFrameCount - mconsumed) * 4);
	memcpy(inBufferR, inBufferR + mconsumed, (inFrameCount - mconsumed) * 4);
}

void AudioResampler32bits::resampleMono32(int32_t* out, size_t outFrameCount,
										  AudioBufferProvider* provider) {
size_t inputIndex = mInputIndex;
    uint32_t phaseFraction = mPhaseFraction;
    uint32_t phaseIncrement = mPhaseIncrement;
    size_t outputIndex = 0;
    size_t outputSampleCount = outFrameCount;
    size_t inFrameCount = (outFrameCount*mInSampleRate)/mSampleRate;
	size_t inIndex = 0;
	int i;

	if(mInSampleRate <  mSampleRate)
		inFrameCount = outFrameCount + 32;
 
	if(!inBufferL)
		inBufferL =  new int32_t[inFrameCount];
	if(!inBufferL)
		return;
	
	if(mconsumed)
		inIndex = inFrameCount- mconsumed;
	else
		inIndex = 0;
    
	while(inIndex < inFrameCount){
		mBuffer.frameCount = inFrameCount - inIndex;
		provider->getNextBuffer(&mBuffer,
								calculateInputPTS(inFrameCount - inIndex));
		if (mBuffer.raw == NULL) {
			mconsumed = 0;
			if(inIndex > 0){
				ALOGVV("mono:padding with zero");
				for(i = inIndex; i < inFrameCount; ++i){
					inBufferL[inIndex] = 0;
				}
				break;
			}else{
				ALOGW("mono:NO DATA TO SRC!");
				return;
			}
		}
		if(mBitDepth == 24){
			uint8_t *in8 =(uint8_t *)mBuffer.i8;
			for(i = 0; i < mBuffer.frameCount; ++i){
				inBufferL[i + inIndex] = (int32_t) (in8[0] << 8 |
													(in8[1] << 16) |
													(in8[2] << 24));
				in8 += 3;
			}
		}else if(mBitDepth == 16){
			int16_t *in16 = mBuffer.i16;
			for(i = 0; i < mBuffer.frameCount; ++i){
				inBufferL[i + inIndex] = (int32_t) (*in16++) << 16 ;
			
			}
		}else if(mBitDepth == 32){
			int32_t *in32 = mBuffer.i32;
			for(i = 0; i < mBuffer.frameCount; ++i){
				inBufferL[i + inIndex] = (int32_t) (*in32++);
			
			}
		}
		
		inIndex += mBuffer.frameCount;
		ALOGVV("mono:GET frames:%d,inIndex:%d", mBuffer.frameCount,inIndex);
		provider->releaseBuffer(&mBuffer);
	}
	mconsumed =
	resampleInt32Mono(out, inBufferL, outFrameCount * 2);

 #ifdef AUDIORESAMPLEER_PCM_DUMP
	FILE *fp;
	if ((fp = fopen("/data/src0l.cap", "a+")) != NULL) {
		fwrite((uint8_t *)inBufferL, 1, mconsumed * 4, fp);
		fclose(fp);
	}else{
			//ALOGVV("src0l.cap OPEN FAILED");
	}	
#endif
	
	memcpy(inBufferL, inBufferL+mconsumed, (inFrameCount - mconsumed) * 4);

}

}
; // namespace android
